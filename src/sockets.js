const Filter = require('bad-words');
const { generateMessage, generateLocationMessage } = require('./utils/messages');
const { addUser, removeUser, getUser, getUsersInRoom } = require('./utils/users');
var con = require('../db/db');
const { io } = require("./server");

io.on('connection', (socket) => {
    console.log('New user websocket connection');


    socket.on('join', (options, callback) => {
        const { error, user } = addUser({ id: socket.id, ...options });
        if (error) {
            return callback(error);
        }
        socket.join(user.room);

        let mySqlQuery = "INSERT INTO  chatroom SET ?";
        con.query(mySqlQuery, user, function (err) {
            if (err)
                throw err;
            console.log('Data inserted !');
        });
        socket.emit('message', generateMessage('Admin', 'Welcome!'));


        socket.broadcast.to(user.room).emit('message', generateMessage('Admin', `${user.username} has joined!`));
        console.log('username -' + " " + user.username + " " + 'room -' + " " + user.room);
        io.to(user.room).emit('roomData', {
            room: user.room,
            users: getUsersInRoom(user.room),
        });
        callback();
    });

   


    socket.on('sendMessage', (message, callback) => {
        const user = getUser(socket.id);
        const filter = new Filter();
        const msg = generateMessage(user.username, message);
        if (filter.isProfane(message)) {
            return callback('Profanity is not allowed!');
        }



        let mySqlQuery = "INSERT INTO  messages SET ?";

        con.query(mySqlQuery , msg, function (err) {
            if (err)
                throw err;
            console.log('messages stored into  database !');
        });
        io.to(user.room).emit('message', msg);
        callback();

    });



    socket.on('sendLocation', (coords, callback) => {

        console.log("sendLocation", coords);
        const user = getUser(socket.id);

        const location = generateLocationMessage(user.username, `https://google.com/maps?q=${coords.latitude},${coords.longitude}`);

        let mySqlQuery = "INSERT INTO  locations SET ?";

        con.query(mySqlQuery, location, function (err) {
            if (err)
                throw err;
            console.log('current location stored into database !');
        });
        io.to(user.room).emit('locationMessage', location);
        callback();
    });



    socket.on('disconnect', () => {
        const user = removeUser(socket.id);
        if (user) {

            // var mySqlQuery = "DELETE FROM chatroom ";
            // con.query(mySqlQuery, function (err, result) {
            //     if (err) throw err;
            //     console.log("Number of records deleted: " + result.affectedRows);
            // });

            io.to(user.room).emit('message', generateMessage('Admin', `${user.username} has left!`));
            io.to(user.room).emit('roomData', {
                room: user.room,
                users: getUsersInRoom(user.room),
            });
            var connectionMessage = user.username + " Disconnected from Socket " + socket.id;
            console.log(connectionMessage);
        }
    });
});
