const mysql = require ('mysql')

var con = mysql.createConnection({
    host: "localhost",
    port:3306,
    user: "root",
    password: "Rajendra@123",
    database: "ChatApp"
  });
  
  con.connect(function(err) {
    if (err) throw err;
    console.log("Database Connected");

    var sql = "CREATE TABLE IF NOT EXISTS chatRoom (id VARCHAR(255), username VARCHAR(255),room VARCHAR(255))"
    con.query(sql, function (err, result) {
      if (err) throw err;
      console.log("Create table chatroom");
    });

    var sql = "CREATE TABLE IF NOT EXISTS messages (username VARCHAR(255), text VARCHAR(255),createdAT VARCHAR(255))"
    con.query(sql, function (err, result) {
      if (err) throw err;
      console.log("Create table messages");
    });

    var sql = "CREATE TABLE IF NOT EXISTS locations (username VARCHAR(255), url VARCHAR(255),createdAT VARCHAR(255))"
    con.query(sql, function (err, result) {
      if (err) throw err;
      console.log("Create table locations");
    });
  });

  module.exports = con;